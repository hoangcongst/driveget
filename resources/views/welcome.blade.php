@extends('layout.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-8">
                <form action="/getlink" method="POST" enctype="multipart/form-data">
                    <input id="token" hidden="true" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label for="name">Link:</label>
                        <input type="text" id="link" class="form-control" name="link" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Your Email:</label>
                        <input type="email" id="email" class="form-control" name="email" required>
                    </div>
                    <input id="submit_button" class="btn btn-primary" type="submit" value="Get Link"/>
                </form>
            </div>
        </div>
    </div>
@endsection